package controllers;

import domain.models.User;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import services.UserService;
import services.interfaces.IUserService;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class BotController extends TelegramLongPollingBot {
    private final IUserService userService = new UserService();
    private final String TOKEN = "1192545406:AAFGz89vt10kKuNtfDxbeiTsfEkT2CPV_b8";
    private final String USERNAME = "aghia7bot";

    @Override
    public void onUpdateReceived(Update update) {
////        part 1
//        Message receivedMessage = update.getMessage();
//        SendMessage sendMessage = new SendMessage();
//        sendMessage.setChatId(receivedMessage.getChatId());
//
//        String name = receivedMessage.getFrom().getFirstName();
//        name = (name != null ? name : "");
//        String surname = receivedMessage.getFrom().getLastName();
//        surname = (surname != null ? surname : "");
//        String welcomeMsg = "Hello, " + name + " " + surname;
//        String question = "Please, enter id of user: ", sendText = "";
//
//        if (receivedMessage.getText().equals("/start")) {
//            sendMessage.setText(welcomeMsg + "\n\n\n" + question);
//        } else {
//            try {
//                long id = Long.parseLong(receivedMessage.getText());
//                User user = userService.getUserByID(id);
//                if (user == null) {
//                    sendText = "User with such id does not exist!";
//                } else {
//                    sendText = user.toString();
//                }
//            } catch (Exception e) {
//                sendText = "User id must be numeric!";
//            }
//            sendMessage.setText(sendText + "\n\n\n" + question);
//        }
//
//
//        try {
//            execute(sendMessage);
//        } catch (TelegramApiException e) {
//            System.out.println(e.getMessage());
//        }

//        part 2
        if(update.hasMessage()) {
            Message receivedMessage = update.getMessage();
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(receivedMessage.getChatId());

            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> table = new ArrayList<>();

            List<InlineKeyboardButton> buttonRaw = new ArrayList<>();
            InlineKeyboardButton btn = new InlineKeyboardButton();

            btn.setText("Hello").setCallbackData("Hello, dear " +
                    receivedMessage.getFrom().getFirstName());
            buttonRaw.add(btn);

            Instant instant = Instant.now();
            btn = new InlineKeyboardButton();
            btn.setText("Time").setCallbackData(instant.toString());
            buttonRaw.add(btn);

            table.add(buttonRaw);

            User user = userService.getUserByID(1);
            buttonRaw = new ArrayList<>();
            btn = new InlineKeyboardButton();
            btn.setText("Author").setCallbackData(user.toString());
            buttonRaw.add(btn);

            btn = new InlineKeyboardButton();
            btn.setText("Goodbye").setCallbackData("Oh, no, please, don`t go!");
            buttonRaw.add(btn);

            table.add(buttonRaw);


            inlineKeyboardMarkup.setKeyboard(table);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            sendMessage.setText("This is test bot...\n\n");
            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                System.out.println(e.getMessage());
            }
        } else if (update.hasCallbackQuery()) {
            try {
                execute(new SendMessage().setText(
                        update.getCallbackQuery().getData())
                        .setChatId(update.getCallbackQuery().getMessage().getChatId()));
            } catch (TelegramApiException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public String getBotUsername() {
        return USERNAME;
    }

    @Override
    public String getBotToken() {
        return TOKEN;
    }
}
